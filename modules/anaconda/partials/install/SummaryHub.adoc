
:experimental:

[[sect-installation-gui-installation-summary]]
=== Installation Summary

The `Installation Summary` screen is the central location for setting up an installation. Most of the options which can be configured during the installation can be accessed from here.

.Installation Summary

image::anaconda/SummaryHub.png[The Installation Summary screen, showing most of the options configured. The Installation Destination icon has a warning sign, which means it requires the user's attention before the installation can proceed.]

[NOTE]
====

If you used a Kickstart option or a boot option to specify an installation repository on a network, but no network is available at the start of the installation, the installer will display the `Network Configuration` screen for you to set up a network connection prior to displaying the `Installation Summary` screen.

====

The summary screen consists of several links to other screens, separated into categories. These links can be in several different states, which are graphically indicated:

* A *warning symbol* (yellow triangle with an exclamation mark) next to an icon means that a screen requires your attention before you start the installation. This typically happens with the `Installation Destination` screen, because even though there is a default automatic partitioning variant, you always have to at least confirm this selection, even if you do not want to make any changes.

* If a link is *greyed out*, it means that the installer is currently configuring this section, and you must wait for the configuration to finish before accessing that screen. This typically happens when you change the installation source in the `Installation Source` screen and the installer is probing the new source location and gathering a list of available packages.

* Screens with *black text and no warning symbol* mean that this screen does not require your attention. You can still change your settings in these screens, but it is not necessary to do so to complete the installation. This typically happens with localization settings, as these are either detected automatically, or set up on the previous screen where you select your language and locale.

A warning message is displayed at the bottom of the summary screen, and the `Begin Installation` button is greyed out, as long as at least one item has not been configured yet.

.Icon States in Installation Summary

image::anaconda/SummaryHub_States.png[A screenshot of several icons in the Installation Summary screen, showing the different states (configured, needs attention, unavailable).]

Each screen also has an explanatory text below its title, showing settings currently configured in that screen. This text may be concatenated; in that case, move your mouse cursor over it and wait until a tooltip with the full text appears.

.Tooltip in the Installation Summary

image::anaconda/SummaryHub_Mouseover.png[An entry in the Installation Summary shows a truncated description and a tooltip with full text.]

Once you configure everything required for the installation, you can press the `Begin Installation` button to start installing {PRODUCT}. This will take you to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-progress[Configuration and Installation Progress]. Note that as the text below this button says, nothing will be written to your hard drive before you press this button. You can press `Quit` at any point; this will discard all changes you made in the installer so far and reboot the system.
